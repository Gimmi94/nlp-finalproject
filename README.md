This repository contains the Big Project assignment for the course of Natural Language Processing A.A. 2018-2019, taught by Professor Roberto Navigli.

Details of the implementation can be read in the report.pdf file.

Note that, in order to execute the Sense Embeddings part, the relative sense embedding should be downloaded from this link http://sensembert.org/ and placed under /resources/sense_embeddings/sensembert/files, with each file named as "sensembert_<2 chars representing the lang>_kb.txt"
