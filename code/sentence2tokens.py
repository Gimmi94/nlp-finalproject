import numpy as np
from typing import List, Dict, Tuple, Any
from tokenizer import FullTokenizer
from tqdm import tqdm


def convert_sentence_to_features(sentence, tokenizer, max_seq_len) -> Tuple[Any, List[int], List[int]]:
    """
    Converts the given sentence to a feature vector
    :param sentence: the sentence to be converted
    :param tokenizer: BERT's tokenizer, which will perform the tokenization process
    :param max_seq_len: the maximum sequence length for this run
    :return: a Tuple containing all the inputs needed for BERt (input_word_ids, input_mask and segment_ids)
    """

    #Applies the tokenization strategy
    tokens  = tokenization_strategy(sentence,tokenizer)

    #Truncates the tokens sequence and makes space for the...
    tokens = tokens[:max_seq_len - 1] if len(tokens) > max_seq_len -1 else tokens

    #... [SEP] token, needed by BERt
    tokens.append('[SEP]')

    #Define the rest of BERt's inputs
    segment_ids = [0] * len(tokens)
    input_ids = tokenizer.convert_tokens_to_ids(tokens)
    input_mask = [1] * len(input_ids)

    zero_mask = [0] * (max_seq_len - len(tokens))
    input_ids += zero_mask
    input_mask += zero_mask
    segment_ids += zero_mask


    return input_ids, input_mask, segment_ids


def tokenization_strategy(sentence:str, tokenizer: FullTokenizer) -> List[str]:
    """
    Actually tokenizes the given sentence
    :param sentence: the sentence to be converted
    :param tokenizer: BERT's tokenizer, which will perform the tokenization process
    :return: a list containing the tokenized sentence
    """
    tokenized_sentence = ["[CLS]"]
    for word in sentence.split():
        tokenized_word = tokenizer.tokenize(word)
        tokenized_sentence.extend(tokenized_word)
    return tokenized_sentence




def convert_sentences_to_features(sentences: List, tokenizer: FullTokenizer, max_seq_len: int =64) -> Tuple:
    """
    Takes in input a list of sequences and translates them to a "Neural Network-friendly" format
    :param sentences: the list of sentences to convert
    :param tokenizer: BERt's tokenizer, to expoit for the conversion
    :param max_seq_len: The maximum sequence length for each string. Default is 64.
    :return: a Tuple containing the 3 inputs required by BERt
    """
    all_input_ids = []
    all_input_mask = []
    all_segment_ids = []
    for sentence in tqdm(sentences):
        input_ids, input_mask, segment_ids = convert_sentence_to_features(sentence, tokenizer, max_seq_len)
        all_input_ids.append(input_ids)
        all_input_mask.append(input_mask)
        all_segment_ids.append(segment_ids)
    return np.asarray(all_input_ids), np.asarray(all_input_mask), np.asarray(all_segment_ids)


def convert_sentence_to_features_with_no_padding(sentence: str, tokenizer: FullTokenizer) -> Tuple:
    """
    Works the same as the convert_sentences_to_features() function, but it has to be used for creating a sentence to be predicted by the network
    :param sentence: the sentence to convert
    :param tokenizer: the BERTs's tokenizer object to use
    :return: the converted sentence
    """
    tokens = tokenization_strategy(sentence,tokenizer)
    tokens.append('[SEP]')

    segment_ids = [0] * len(tokens)
    input_ids = tokenizer.convert_tokens_to_ids(tokens)
    input_mask = [1] * len(input_ids)

    return input_ids, input_mask, segment_ids

def convert_sentence_to_features_no_padding(sentence: str, tokenizer: FullTokenizer) -> Tuple:
    """
    Performs the same action of convert_sentences_to_features() on a single sentence, but without adding padding.
    :param sentence: the sentence to convert
    :param tokenizer: the BERTs's tokenizer object to use
    :return: a Tuple containing the 3 inputs required by BERt
    """
    input_ids, input_mask, segment_ids = convert_sentence_to_features_with_no_padding(sentence, tokenizer)

    return np.asarray([input_ids]), np.asarray([input_mask]), np.asarray([segment_ids])

def convert_y(label: List[List[str]],
              label_bn: Dict,
              label_wndmn: Dict,
              label_lex: Dict,
              tokenizatore: FullTokenizer,
              train_data: List[str],
              max_seq_len:int = 64) -> Tuple:
    """
    Converts a given list of list of labels  to a "Neural Network-friendly" format
    :param label: The List of List that contains the labels. Position 0 is for BN labels, 1 is for WordNet Domains Labels and 2 is for Lexicon
    :param label_bn: the dictionary for mapping BabelNet Synset IDs
    :param label_wndmn: the dictionary for mapping Wordnet Domains Synset IDs
    :param label_lex: the dictionary for mapping Lexicon Synset IDs
    :param tokenizatore: the BERTs's tokenizer object to use
    :param train_data: the received dataset. It is needed for a punctual mapping of sentences, in case of subwords
    :param max_seq_len: The maximum sequence length for each string. Default is 64.
    :return: a Tuple containing the 3 labels converted

    """
    label_bn_to_return = []
    label_wndmn_to_return = []
    label_lex_to_return = []
    for sent_arr, sent in tqdm(zip(label, train_data)):
        #Consulting the needed dictionary for each task for making a punctual mapping
        sent1 = __consult_dict(sent_arr[0],label_wndmn, max_seq_len, tokenizatore, sent)
        sent2 = __consult_dict(sent_arr[1], label_bn, max_seq_len, tokenizatore, sent)
        sent3 = __consult_dict(sent_arr[2], label_lex,max_seq_len, tokenizatore, sent)
        label_wndmn_to_return.append(sent1)
        label_bn_to_return.append(sent2)
        label_lex_to_return.append(sent3)
    return np.asarray(label_bn_to_return), np.array(label_wndmn_to_return), np.array(label_lex_to_return)



def __consult_dict(sentence: str,
                   vocab: Dict,
                   max_seq_len: int,
                   tokenizatore: FullTokenizer,
                   sent_original: str) -> List:
    """
    Support function for the convert_y() function.
    By taking in input the label sentence, the label gets converted to a "Neural Network-friendly" format
    :param sentence: the label to convert
    :param vocab: the dictionary to exploit for the given task (can be BN2WN, BN2WNDMN or BN2LEX)
    :param max_seq_len: The maximum sequence length for each string
    :param tokenizatore: the BERTs's tokenizer object to use
    :param sent: the original sentence before being labeled
    :return: a List containing the translation for the Neural Network
    """
    to_return = []

    #Tokenize the original sentence by space
    orig_sent_splitted = sent_original.split(" ")

    #Iterates over the labeled sentence splitted by space...
    for index, parola in enumerate(sentence.split(" ")):
        to_use = None
        try:
            #...so that it can be possible to search for that word in the given dictionary
            to_use = vocab[parola][0]
        except:
            #...or get a 'DEFAULT' token if the word is not in the dictionary
            to_use = vocab['<UNK>']

        #And, before continuing the iteration, insert as many to_use as needed per the tokenization of the original word, before labelling it.
        #This allows to have an exact mapping between sentence and mapping.
        # Ie, if the sentence "fantastic!" is tokenized in
        # "fanta,##stic"
        # And then encoded in
        # 3123,42131
        # And the label is "fantastic_bn:3123123", the encoding would be missing fantastic's subwords
        # 3123
        # But with this for loop, the root's ID will be added in place of the subword. This is done in order to emphatize the root of the word, which usually contains more explicative power
        # So, in the end, the mapping would be
        # 3123, 3123
        # And, so, len(tokenized_sentence_ids) = len(labeled_sentence_ids)
        for _ in tokenizatore.tokenize(orig_sent_splitted[index]):
            to_return.append(to_use)
    #Truncating the sentence if it exceeds the maximum length
    to_return = to_return[:max_seq_len - 1] if len(to_return) > max_seq_len -1 else to_return

    #And adding padding, if needed
    zero_mask = [0] * (max_seq_len - len(to_return))
    to_return.extend(zero_mask)
    return to_return


if __name__ == "__main__":
    test = "ciao come va?"
    again = "Hello, this is such a wonderful night. Isn't it?"
    againagain = "Halo, icch habbe Gianmarco. Du urh dast?"
    tokenizatore = FullTokenizer("/Users/gimmi/Desktop/Università/MAGISTRALE/NLP/nlp-finalproject/code/cache/a7f4eb577e5eeec24c73b9dace49639b7c8193ed/assets/vocab.txt", do_lower_case=False)
    print(tokenization_strategy(test, tokenizatore))
    print(tokenization_strategy(again, tokenizatore))
    print(tokenization_strategy(againagain, tokenizatore))