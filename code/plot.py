import matplotlib.pyplot as plt
from typing import Dict
import pickle

def load_pckl(path_to_metrics: str) -> Dict:
    """
    Loads the given pckl and returns it as a dictionary
    :param path_to_metrics: the path to the given pckl file
    :return:
    """
    with open(path_to_metrics, "rb") as metriche_codificate:
        metriche_parsate = pickle.load(metriche_codificate)
    return metriche_parsate


def plot_loss(metric_dictionary: Dict, save_on_disk: bool = False) -> None:
    """
    By taking the history's object dictionary in input, this function outputs a plot, with the progress of the loss of the model
    during the train phase. This function assumes that the given dictionary is of type {'loss':[some content]}
    :param metric_dictionary: A dictionary that holds the loss and validation loss values
    :param save_on_disk: if True, the plot is saved on the File System. If False, it will be shown in stead
    :return: //
    """

    #Defining the container of the plot
    _, axis = plt.subplots(1, figsize=(6, 4))

    #Defining the labels of the two axes
    axis.set_ylabel('Loss', fontsize=10)
    axis.set_xlabel('Val Loss', fontsize=10)

    #Plotting stuff
    axis.plot(metric_dictionary['loss'])
    axis.plot(metric_dictionary['val_loss'])

    #Defining the title
    axis.set_title('loss')

    #Defining the legend
    axis.legend(
        ["loss", "val_loss"],
        loc="upper right",
        frameon=False,
        fontsize="medium",
    )

    #Show time
    if save_on_disk:
        plt.savefig("loss.png")
    else:
        plt.show()
