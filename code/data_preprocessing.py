from typing import List, Dict, Tuple
from collections import defaultdict, Counter, OrderedDict
from nltk.corpus import wordnet as wn
import nltk
from lxml import etree
import os
from tqdm import tqdm


def __check_if_file_exists(filename: str) -> bool:
    """
    Check if the file already exists parsed
    :param filename: the name of the file to look for
    :return: True if the file is already in the resources directory, parsed. False otherwhise.
    """
    return any(files for root, dirs, files in os.walk("../") if filename in files)


def __start_dataset_parsing(filepath: str):
  """
  Parse the given filepath and returns an lxml etree object.
  :param filepath: the path of the file to parse
  :return its representation as an lxml etree object
  """
  with open(filepath, "rb") as infile:
    tree_train = etree.fromstring(infile.read())
  return tree_train


def load_dataset(filepath: str) -> Tuple:
    """
    Loads a given file from input and returns it as a List of List. This function supposes that the incoming input is a .xml file and that the path
    actually brings to the file
    :param filepath_train: the either relative or absolute path
    :return: a List of List that contains the parsed data and the etree object that will have to be passed to the load_gold_key_file, if a label file will be supplied
    """

    #Checks whether the file has already been parsed...
    filename = filepath[filepath.rfind('/') + 1:-3] + "casted.txt"
    already_parsed = __check_if_file_exists(filename)

    #Loads the lxml object. This is needed anyways, so it's ok to leave it here
    tree_train = __start_dataset_parsing(filepath)

    #If the file was actually parsed and already present in the file system...
    if already_parsed:
        print("File was already parsed. Reloading it...")
        #Then reload it
        filepath = "../resources/parsed/"+filename if 'dev' not in filepath else "../resources/parsed/dev/"+filename
        data = __reload_data_from_disk(filepath, data='train')
        return data, tree_train
    print("Parse dataset starting...")
    #Else, parse the just received input
    sentences = __load_train_sentences(tree_train)

    #Removes any possible whitespace
    train= [sentence.strip() for sentence in sentences]

    #And save the finished file
    with open('../resources/parsed/'+filename, 'a') as to_save:
        for frase in train:
            to_save.write("#"+frase+"#"+"\n")
    print("Done. Returning...")
    return train, tree_train


def load_gold_key_file(filepath: str, tree_train: etree._Element) -> List[List]:
    """
    Loads a given file from input and returns it as a List. This function supposes that the incoming input is a .txt file and that the path
    actually brings to the file. The file is considered to be the gold.key of the previously loaded file
    :param filepath_train: the either relative or absolute path
    :return: a List of List that contains the parsed data
    """

    #Checks whether the file has already been parsed...
    filename = filepath[filepath.rfind('/') + 1:-3] + "casted.txt"
    print("Checking ", filename)
    already_parsed = __check_if_file_exists(filename)

    #If the file was actually parsed and already present in the file system...
    if already_parsed:
        filepath = "../resources/parsed/"+filename if 'dev' not in filepath else "../resources/parsed/dev/"+filename
        data = __reload_data_from_disk(filepath, data='test')
        print("Done loading gold key")
        return data
    with open(filepath, encoding="utf-8") as infilez:
        contents_label = infilez.read().splitlines()

    #Removes any possible "dirty" character
    contents_label = [label for label in contents_label if label != "" and label != "\n" or label is not None]

    #Create the gold key file's representation in memory
    pool_of_labels = {key.split(" ")[0] : key.split(" ")[1] for key in contents_label}

    #Generates the actual labels array
    labels = __create_labeled_sentences(pool_of_labels,tree_train, '../resources/parsed/'+filename)
    print("Done with labels...")
    return labels

def __reload_data_from_disk(filepath: str, data: str ='train'):
    """
    Reload the previously parsed files from disk
    :param filepath: the path of the file to be loaded
    :param data: whether the file is for train or other purposes
    :return: a List (if data == 'train') or a List of List (if data != 'train') containing the data of the loaded file
    """
    with open(filepath, 'r') as writer:
        content = writer.read()
    to_return = []
    if data == 'train':
        to_return = [word for word in content[1:-1].split('#') if word and word is not '\n']
        return to_return
    for word in content[1:-1].split('#'):
        if not word or word == '' or word == '\n':
            continue
        tmp = [frase for frase in word.split('~')]
        to_return.append(tmp)
    return to_return


def __load_train_sentences(tree : etree._Element ) -> List:
    """
    Parse the lxml tree object to get the train sentences
    :param tree: the parsed_yeah .xml objet
    :return: the list containing all the train sentences
    """
    to_return = []
    #Gather all the elements annotated with the <sentence> tag.
    #This can work because the first occurrence of <sentence> happens to be the child of the third node
    sentences_xml_elements = tree.xpath("/*/*/*")
    for sentence_xml in sentences_xml_elements:
        children = sentence_xml.getchildren() #Get all the <wf> and <instance> tags for that sentence
        phraseToBuild = ' '.join([word.text for word in children]) #Build the actual phrase through a list comprehension
        to_return.append(phraseToBuild)
    return to_return


def __create_labeled_sentences(pool_of_labels: dict, tree : etree._Element, filepath: str) -> List[List]:
    """
    Creates the labels for the model
    :param pool_of_labels: the gold key file parsed_yeah to a dict
    :param tree: the parsed_yeah .xml file
    :param filepath: where to save the output of this function
    :return: a List of List which contains all the labeled sentences
    """
    print("Saving labeled stuff")
    to_return = []
    mapping_to_persist = defaultdict(list)

    #This can work because the first occurrence of <sentence> happens to be the child of the third node
    sentences_xml_elements = tree.xpath("/*/*/*")
    for sentence in tqdm(sentences_xml_elements):
        #Iteratively, create 3 sentences: one for each task that the Neural Network will later perform
        wn_domains_sentence = ""
        bn_domains_sentence = ""
        lex_domains_sentence = ""
        for word in sentence:
            name = word.xpath("name()")

            #Avoid to take empty tag elements that, sometimes, lxml generates
            if name is not None or name is not "":
                if name == 'wf':

                    #If the system is checking a <wf> tag, the worfklow of the prhase's building is still the same
                    wn_domains_sentence += ' ' + word.text
                    bn_domains_sentence += ' ' + word.text
                    lex_domains_sentence += ' ' + word.text
                    continue
                if name == 'instance':

                    #If the system is checking an <instance> tag, instead...
                    id = word.get('id')
                    sense_key = pool_of_labels[id]

                    #I need to convert the wordnet synset key to...
                    wordnet_synset = wn.lemma_from_key(sense_key).synset()
                    synset_id = "wn:" + str(wordnet_synset.offset()).zfill(8) + wordnet_synset.pos()

                    #...a WordNet Domains Synset Key...
                    wn_domains_sentence += ' ' + put_correct_id(word.get('lemma'),synset_id,mode='wn_dom')

                    #...a BabelNet Synset Key...
                    bn_domains_sentence += ' ' + word.get('lemma') + '_' + put_correct_id(word.get('lemma'),synset_id)

                    #...or a Lexicon Synset Key
                    lex_domains_sentence += ' ' + put_correct_id(word.get('lemma'),synset_id,mode='lex')

                    #A dictionary that will be useful for mapping predicted words. I'll append every synset id that I find for that word
                    mapping_to_persist[word.text].extend([synset_id])
                    continue
            else:
                continue
        to_return.append([wn_domains_sentence.strip(),bn_domains_sentence.strip(),lex_domains_sentence.strip()])
        print("Fine della sentence")
    print("Scrivo su file...")

    #Saving the gold key parsed file in the file system
    with open(filepath, 'a') as writer:
        for matrix in to_return:
            writer.write('#' + matrix[0].strip() + '~' + matrix[1].strip() + '~' + matrix[2].strip() + '#' +"\n")

    #And the lemma 2 synset key as well
    with open('../resources/mapping/lemma2wn.txt','a') as mapping_lemma:
        for chiave, valore in mapping_to_persist.items():
            stringa = ' '.join(wn_id for wn_id in valore)
            mapping_lemma.write(chiave + "~" + stringa+"#\n")
    print("end")
    return to_return


def put_correct_id(word:str, synset_id: str, mode: str='bn') -> str:
    """
    Finds the correct synset id for the given id
    :param word: the word that the system is evaluating right now
    :param synset_id: the id associated with the current :word
    :param mode: the id that I have to look for. Possible options: BabelNet (bn), WordNet Domains (wn_dom), LexNames (lex)
    :return: the found id if exists, else the word that the system is looking in this exact moment
    """
    # next() seems to be way faster than a classic list comprehension
    synset_bn = next(key for key, value in bn2wn.items() if synset_id in value)
    if mode == 'wn_dom':
        try:
            synset_wn_dom = bn2wndomains[synset_bn][0]
            return synset_wn_dom
        except:
            #Mapping to factotum if nothing is found
            return "factotum"
    if mode == 'bn':
        return synset_bn
    else:
        try:
            synset_lex = bn2lex[synset_bn][0]
            return synset_lex
        except IndexError:
            #Assuring at least a sense
            return word
    return word


def create_mapping_dictionary(resources_path: str,
                              data = None,
                              max_size_of_vocab: int = 30000,
                              min_count: int = 3,
                              mode: str="bn") -> Dict:
    """
    Creates (or reload from disk) the mapping label dictionary of the given mode and file
    :param resources_path: the path where the program should go to look for files
    :param data: The collection that will represent the data from where create the dictionary. This can be None (so, default type) if I just want to "force" the load of something from disk
    :param max_size_of_vocab: max size of the vocab. Default is 30k
    :param min_count: min count of the vocab. Default is 3
    :param mode: whether I need to handle the #bn ids or #lex ids or #wndmn ids
    :return: a Dictionary that goes by word -> serial

    """

    #Just to be 100% sure...
    try:
        #Check if Wordnet is in the F.S.
        nltk.data.find('wordnet')
        print("WordNet already here")
    except:
        #If it's not, it installs it
        print("Installing WordNet...")
        nltk.download('wordnet')

    print("Creating Mapping dictionary....")
    filename = "/vocabularies/"

    #Adding info to the filename variable depending on the chosen mode
    if mode == 'wndmn' or mode == 'lex':
        filename += "vocab_wn_domains_labels.txt" if mode == 'wndmn' else "vocab_lex_labels.txt"
    if mode == 'bn':
        filename += "vocab_bn_labels.txt"

    #If this process has already been performed, no need to go all over again
    if os.path.exists(resources_path+filename) and data is None:
        #the mapping gets reloaded and the function returns it
        to_return = __load_vocab_mapping(resources_path+filename)
        print("Done loading dictionary")
        return to_return

    #Counting the number of words
    count_of_words = __count_words(data,mode=mode)

    #And orders it
    count_of_words = OrderedDict(
        sorted(count_of_words.items(), key=lambda dict_record: int(dict_record[1]), reverse=True)
    )

    #Create a temporary dictionary that will only contain, at maximum, max_size_of_vocab_words
    i = 0
    tmp_container = dict()
    for key, value in count_of_words.items():
        if i > max_size_of_vocab:
            #I've reached the max consented length of the dictionary. No need to continue
            break
        tmp_container[key] = value
        i += 1
    i = 2

    #Cutting the dictionary to words that match the minimum count
    tmp_container = {word: counter for word, counter in tmp_container.items() if counter >= min_count}
    to_return = {'<PAD>':0, '<UNK>':1} #Adding predefined tokens

    #Building the final mapping dictionary...
    for chiave, valore in tmp_container.items():
        if chiave not in to_return:
            to_return[chiave] = i
            i += 1
        else:
            continue

    #Saves the just created mapping
    if not os.path.exists(resources_path):
        os.mkdir(resources_path)
    with open(resources_path+filename, "w+") as fileToWrite:
        for chiavi, valori in to_return.items():
            fileToWrite.write(chiavi + " " + str(valori) + "\n")
    return to_return


def load_multilingual_mapping(filepath: str, lang: str='EN') -> Dict:
    """
    Given the base path, looks for the multilingual mapping provided, parse it, and return the 5 dictionaries of the languages (IT,FR,DE,ES)
    :param filepath: the first part of the path that needs to be used to load the file
    :param lang: The language to which pay special attention in the parsing process. Possible values:
        EN --> English will be the only language considered
        IT --> Italian will be the only language considered
        DE --> Deutsch will be the only language considered
        ES --> Spanish will be the only language considered
        FR --> French will be the only language considered
        N.B.: These are just an example basing on the slides given as a guideline. All languages are welcome thanks to the implementation.
    :return: a dictionary that actually represent the mapping of the language
    """
    language_dict = defaultdict(list)
    try:
        #Check if the Open Multilingual Wordnet is in the F.S.
        nltk.data.find('corpora/omw.zip')
        print("OMW already here")
    except:
        #If it's not, it installs it
        print("Installing OMW...")
        nltk.download('omw')

    try:
        #Check if Wordnet is in the F.S.
        nltk.data.find('wordnet')
        print("WordNet already here")
    except:
        #If it's not, it installs it
        print("Installing WordNet...")
        nltk.download('wordnet')

    with open(filepath) as fileToRead:
        content = fileToRead.readlines()
    for riga in content:
        contenuto_interno = riga.split()
        lemma = contenuto_interno[1]
        lemma = lemma[:lemma.rfind('#')]
        if contenuto_interno[0] == lang:
            language_dict[lemma].extend(contenuto_interno[2:])
    return language_dict

def __count_words(data, mode: str = "bn") -> Counter:
    """
    Calculate the number of words and saves it in a file with a counter given by the Counter object
    :param data: the data where to count the words. Can either be a list or a list of list.
    :param max_size_of_vocab: max size of the vocab. Usually goes for 30k
    :param min_count: min count of the vocab. Usually is 3
    :param mode: whether I need to count the  #bn ids or #lex ids or #wndmn ids
    :return: the count inside a Counter object
    """
    count_of_words = None
    if mode == 'wndmn' or mode == 'lex':
        #BabelNet is placed in the first position (0), WordNet Domains are placed in the second position (1)
        # and Lexicon in the third one (2)
        index_to_choose = 1 if mode == 'wnmdn' else -1
        data = [[line for line in test] for test in data]
        data= [dato[index_to_choose] for dato in data if dato]
    if mode == 'bn':
        data = [[line for line in test if line and  "bn:" in line] for test in data]
        data= [dato[0] for dato in data if dato]
    count_of_words = Counter(word for line in data for word in line.split(" "))
    return count_of_words


def __load_synset_mapping(filepath: str) -> Dict:
    """
    Loads the synset mapping from the system
    :param filepath: the path of the file to load
    :return: a dict representation of the loaded file
    """
    dizionario = defaultdict(list)
    with open(filepath) as file:
        content = file.readlines()
    for arr in content:
        key = arr.split()[0]
        value = arr.split()[1:]
        dizionario[key].extend(value)
    return dizionario


def __load_vocab_mapping(filepath: str) -> Dict:
    """
    Loads the vocab mapping from the system
    :param filepath: the path of the file to load
    :return: a dict representation of the loaded file
    """
    print("Reloading dict from disk")
    dizionario = dict()
    with open(filepath) as file:
        content = file.readlines()
    for arr in content:
        key = arr.split()[0]
        value = arr.split()[1:]
        dizionario[key] = value[0]
    return dizionario


def reload_word_mapping(filepath:str) -> Dict:
    """
    Loads the lemma to wn id mapping from the FS
    :param filepath: the path of the file to load
    :return: a dict representation of the loaded file
    """
    dizionario = dict()
    with open(filepath) as file:
        content = file.read()
    for arr in content.split('#'):
        if arr == '\n':
            continue
        try:
            key = arr.split('~')[0]
            key =key[1:] if '\n' in key else key
            value = arr.split('~')[1:][0].split(' ')
        except:
            print(f"Exception happened while iterating on {arr}")
        dizionario[key] = value
    return dizionario


bn2wn = __load_synset_mapping('../resources/babelnet2wordnet.tsv')
bn2wndomains = __load_synset_mapping('../resources/babelnet2wndomains.tsv')
bn2lex = __load_synset_mapping('../resources/babelnet2lexnames.tsv')


def get_bn2wn() -> Dict[str,list]:
    """
    Returns the Babelnet Synset Keys to Wordnet Synset Keys dictionary
    :return: the dictionary containing babelnet synset keys -> wordnet sysnet keys
    """
    return bn2wn

def get_bn2wndomains() -> Dict[str,list]:
    """
    Returns the Babelnet Synset Keys to Wordnet Domains Synset Keys dictionary
    :return: the dictionary containing babelnet synset keys -> wordnet domains sysnet keys
    """
    return bn2wndomains

def get_bn2lex() -> Dict[str,list]:
    """
    Returns the Babelnet Synset Keys to Lexicon Synset Keys dictionary
    :return: the dictionary containing babelnet synset keys -> Lexicon sysnet keys
    """
    return bn2lex

if __name__ == "__main__":
    train,etree_file = load_dataset("../dataset/SemCor/semcor.data.xml")
    label = load_gold_key_file("../dataset/SemCor/semcor.gold.key.txt", etree_file)
    train = [dato for dato in train if dato and dato]
    #vocab_label_bn = create_mapping_dictionary_reloaded("../dataset/SemCor/semcor.gold.key.txt","../resources", etree_file,mode="bn")
    #vocab_label_wndmn = create_mapping_dictionary_reloaded("../dataset/SemCor/semcor.gold.key.txt","../resources", etree_file,mode="wn_dom")
    #vocab_label_lex  = create_mapping_dictionary_reloaded("../dataset/SemCor/semcor.gold.key.txt","../resources", etree_file,mode="lex")
    #vocab_train = create_mapping_dictionary("../resources",data = train)
    vocab_label_bn = create_mapping_dictionary("../resources", data = label, max_size_of_vocab = 30000, min_count = 5,mode='bn')
    vocab_label_wndmn = create_mapping_dictionary("../resources", data = label, max_size_of_vocab = 30000, min_count = 5,mode='wndmn')
    vocab_label_lex = create_mapping_dictionary("../resources", data = label,max_size_of_vocab = 30000, min_count = 5, mode='lex')