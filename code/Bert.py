import torch
from transformers import BertModel, BertConfig, BertTokenizer
from sentence2tokens import convert_sentences_to_features
import numpy as np



class Bert:
    def __init__(self, is_it_english: bool = True):
        """
        Initialize a class that will contain BERT's implementation of HuggingFaces.
        :param is_it_english: whether the system will perform WSD on an english corpora. Default is true
        """

        #Understand which BERT should be downloaded
        bert_to_use = 'bert-large-cased' if is_it_english else 'bert-base-multilingual-cased'

        #Initializing all the BERT related things...
        self.tokenizer = BertTokenizer.from_pretrained(bert_to_use)

        #By setting output_hidden_states to True, it will be possible to access to the hidden layers
        config = BertConfig.from_pretrained(bert_to_use, output_hidden_states=True)
        self.model = BertModel.from_pretrained(bert_to_use, config=config)
        self.model.eval()


    def predict(self, v1,v2) -> np.ndarray:
        """
        Takes in input the two vectors required for BERT to do its prediction and performs it.
        :param v1: The input_word_ids vector
        :param v2: the segment_ids vector
        :return: an nd.array containing the prediction results from BERT
        """
        print("Starting BERT prediction...")

        #Initializing Torch Tensors from the numpy arrays received...
        vec1 = torch.from_numpy(v1)
        vec2 = torch.from_numpy(v2)

        #Prediction phase...
        outputs = self.model(vec1,vec2)

        #Sum the last two layers of the output
        _e1 = [outputs[2][i] for i in (-1, -2)]
        _e2 = torch.cat(_e1,2)
        _e2 = torch.sum(_e2,dim=0).unsqueeze(0)

        #Returning the result as a numpy ndarray
        return _e2.detach().numpy()



if __name__ == "__main__":
    from data_preprocessing import load_dataset, load_gold_key_file, create_mapping_dictionary
    from sense_embeddings_preprocessing import load_senses
    tokenizer = BertTokenizer.from_pretrained('bert-base-cased')
    bert = Bert("https://tfhub.dev/tensorflow/bert_en_wwm_cased_L-24_H-1024_A-16/1","/Users/gimmi/Desktop/Università/MAGISTRALE/NLP/nlp-finalproject/resources/vocabularies/bert_vocab.txt",1024)
    train,etree_file = load_dataset("../dataset/SemCor/semcor.data.xml")
    label = load_gold_key_file("../dataset/SemCor/semcor.gold.key.txt", etree_file)
    train = [dato for dato in train if dato and dato]

    train_1, train_2, train_3, _,_  = convert_sentences_to_features(train,tokenizer,max_seq_len=64)

    print("train_1", train_1.shape)
    print("train_2", train_2.shape)
    print("train_3", train_3.shape)

    sentences_xml_elements = etree_file.xpath("/*/*/*")


    sequence_output = bert.predict(train_1, train_2, train_3)
