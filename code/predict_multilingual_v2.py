from data_preprocessing import load_multilingual_mapping, get_bn2wn, load_dataset
from Bert import Bert
from predict import __decide_pos
from typing import Dict, Tuple, Optional
from sklearn.metrics.pairwise import cosine_similarity
from nltk.corpus import wordnet
from sense_embeddings_preprocessing import load_senses
import numpy as np
from sentence2tokens import convert_sentence_to_features_no_padding
import os
from transformers import BertTokenizer
from tqdm import tqdm



def predict_multilingual(input_path: str, output_path: str, resources_path: str, lang: str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <BABELSynset>" format (e.g. "d000.s000.t000 bn:01234567n").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :param lang: the language of the dataset specified in input_path
    :return: None
    """
    global mfs_counter
    print(">>>> BABELNET MULTILANG PREDICTION")
    lang_to_exploit = "ES" if lang == "spa" else lang.upper()[0:2]
    prediction_results, sentences_xml_elements, tokenizzatore = __predict(input_path, lang)
    sense_dictionary = load_senses(resources_path+"/sense_embeddings/sensembert_files/sensembert_" + lang_to_exploit +"_kb.txt", resources_path, is_it_english=True if lang_to_exploit == "EN" else False)
    filename = os.path.normpath(input_path)
    filename = filename.split(os.sep)[-1]
    filename = filename[:-3] + "babelnet_" + lang + ".gold.key.txt"
    lang_vocab = load_multilingual_mapping(resources_path + "/mapping/lemma2synsets4.0.xx.wn.ALL.txt",
                                           lang=lang_to_exploit)
    for index in tqdm(range(len(prediction_results))):
        __write_result_multilang(
                                  filename,
                                  sentences_xml_elements[index],
                                  output_path,
                                  prediction_results[index][0][1:-1],
                                  tokenizzatore,
                                  sense_dictionary,
                                  lang_vocab,
                                  lang)

    print("Success!")
    return


def __write_result_multilang(filename: str,
                             frase,
                             outputh_path: str,
                             predictions,
                             tokenizer,
                             sense_dictionary: Dict,
                             lang_dict: Dict,
                             lang: str) -> None:

    """

    :param filename: the name of the file to save
    :param frase: the object from which recover the sentence
    :param outputh_path: the path where to save the results
    :param predictions: the predictions made by the model
    :param tokenizer: BERT's tokenizer
    :param sense_dictionary: the senses dictionary
    :param lang_dict: the language dictionary
    :param lang: the language that the system is evaluating
    :return: //
    """

    bn2wn = get_bn2wn()
    to_write = []
    count = 0
    for index, parola in enumerate(frase):
        name = parola.xpath("name()")
        if name == 'instance':
            id = parola.get('id')
            list_of_possible_senses_first_step = lang_dict.get(parola.get('lemma'))
            if not list_of_possible_senses_first_step:
                # MFS
                the_actual_meaning = MFS(parola,
                                         bn2wn,
                                         lang,
                                         id)
                if the_actual_meaning is None:
                    continue
                to_write.append((id, the_actual_meaning))
                continue
            pooled_stuff = np.mean(
                predictions[count: count + len(tokenizer.tokenize(parola.text))], axis=0
            )
            count += len(tokenizer.tokenize(parola.text))

            the_actual_meaning = None

            try:
                list_of_sense_embeddings = [sense_dictionary.get(parola.get('lemma')).get(senso)['embeddings'][0] for senso in
                                            set(list_of_possible_senses_first_step) if sense_dictionary.get(parola.get('lemma'))
                                            and sense_dictionary.get(parola.get('lemma')).get(senso)]
                prova = np.array(list_of_sense_embeddings)
                prob_sensi = cosine_similarity(pooled_stuff.reshape(1,-1), prova)
                if prob_sensi.mean() > 0.5:
                    the_actual_meaning = list_of_possible_senses_first_step[np.argmax(prob_sensi)]
                    to_write.append((id, the_actual_meaning))
                    continue
            except:
                print(f"Exception for {parola.text}")
                pass

            if not the_actual_meaning:
                # MFS
                the_actual_meaning = MFS(parola,
                                         bn2wn,
                                         lang,
                                         id)
                if the_actual_meaning is None:
                    continue
                else:
                    to_write.append((id, the_actual_meaning))
                    continue
            to_write.append((id, the_actual_meaning))
        else:
            count += len(tokenizer.tokenize(parola.text))
    with open(outputh_path + "/" + filename, "a") as test_saving:
        for tupla in to_write:
            test_saving.write(tupla[0] + " " + tupla[1] + "\n")

    return None


def MFS(parola, vocab: Dict, lang: str, id:str) -> Optional[str]:
    """
    Returns the sense by applying the Most Frequent Sense (MFS) strategy with a multilingual approach
    :param parola: the word to use for the MFS approach
    :param vocab: the babelnet 2 wordnet dictionary
    :param lang: the language currently used
    :param id: the ID of the synset in the corpus
    :return: the chosen sense with the MFS technique
    """

    pos = parola.get('pos')
    pos_input = __decide_pos(pos)
    language_to_use = next(language for language in wordnet.langs() if language[0:2] == lang or language == lang)
    wordnet_object = wordnet.synsets(parola.get('lemma'), pos=pos_input, lang=language_to_use)
    try:
        wordnet_object = wordnet_object[0]
    except:
        print("----------------------------")
        print(wordnet_object)
        print(parola.get('lemma'))
        print(pos_input)
        print(language_to_use)
        print(id)
        print("----------------------------")
        return None
    wn_synset = "wn:" + str(wordnet_object.offset()).zfill(8) + wordnet_object.pos()
    the_actual_meaning = next(key for key, value in vocab.items() if wn_synset in value)
    if type(the_actual_meaning) == str:
        return the_actual_meaning
    if type(the_actual_meaning) == list:
        return the_actual_meaning[0]


def __predict(input_path : str, lang:str ) -> Tuple:

    """
    Actually predicts a sentence and returns the predictions in the requested formats
    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: The actual prediction by the network
    """
    train, etree_data = load_dataset(input_path)
    train = [dato for dato in train if dato]
    bert = Bert(is_it_english=True if lang == "EN" else False)
    tokenizatore = bert.tokenizer
    sentences_xml_elements = etree_data.xpath("/*/*/*")
    to_return = []
    for sent in tqdm(train):
        train_1, train_2, train_3 = convert_sentence_to_features_no_padding(sent, tokenizatore)
        output = bert.predict(train_1, train_2)
        to_return.append(output)
        print("Prediction done.")
    return to_return,sentences_xml_elements, tokenizatore


if __name__ == "__main__":
    predict_multilingual(
        "/Users/gimmi/Desktop/Università/MAGISTRALE/NLP/nlp-finalproject/dataset/eval_dataset/semeval2013.it.data.xml",
        "../output/output2", "../resources", "ita")
