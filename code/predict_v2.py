from data_preprocessing import load_dataset, create_mapping_dictionary, reload_word_mapping, get_bn2wn, get_bn2wndomains, get_bn2lex
from typing import List, Dict, Tuple
from sense_embeddings_preprocessing import load_senses
from tokenizer import FullTokenizer
from sentence2tokens import tokenization_strategy
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
import os
from nltk.corpus import wordnet
from tqdm import tqdm
from Bert import Bert


def predict_babelnet(input_path: str, output_path: str, resources_path: str) -> None:
    global mfs_counter
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <BABELSynset>" format (e.g. "d000.s000.t000 bn:01234567n").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    print(">>>> BABELNET PREDICTION")
    prediction_results, sentences_xml_elements, tokenizzatore = __predict(input_path, resources_path)
    sense_dictionary = load_senses(resources_path+"/sense_embeddings/sensembert_files/sensembert_EN_kb.txt", resources_path, is_it_english=True)
    vocab_label_bn = create_mapping_dictionary(resources_path, mode='bn')
    filename = os.path.normpath(input_path)
    filename = filename.split(os.sep)[-1]
    filename = filename[:-3] + "babelnet.gold.key.txt"
    for index in tqdm(range(len(prediction_results))):
        __write_result(filename,
                      sentences_xml_elements[index],
                      resources_path, output_path,
                      prediction_results[index][0][1:-1],
                      tokenizzatore,
                      sense_dictionary,
                      vocab=vocab_label_bn,
                      enable_coarse_grained=1,
                      vocab_for_coarse=None)
    print("Success!")
    return


def predict_wordnet_domains(input_path: str, output_path: str, resources_path: str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <wordnetDomain>" format (e.g. "d000.s000.t000 sport").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    global mfs_counter
    print(">>>> WORDNET DOMAINS  PREDICTION")
    prediction_results, sentences_xml_elements, tokenizzatore = __predict(input_path, resources_path)
    vocab_label_wndmn = create_mapping_dictionary(resources_path, mode='wndmn')
    correctly_saved = 0
    bn2wndom = get_bn2wndomains()
    filename = os.path.normpath(input_path)
    filename = filename.split(os.sep)[-1]
    filename = filename[:-3] + "wndomains.gold.key.txt"
    for index in tqdm(range(len(prediction_results))):
        correctly_saved += __write_result(filename,
                                          sentences_xml_elements[index],
                                          resources_path, output_path,
                                          prediction_results[index][1][0][1:-1],
                                          tokenizzatore,
                                          vocab=vocab_label_wndmn,
                                          enable_coarse_grained=2,
                                          vocab_for_coarse=bn2wndom)

    print("Successfully saved {} out of {}".format(correctly_saved, len(prediction_results)))
    del prediction_results
    print("Of these, {} were MFS".format(mfs_counter))
    mfs_counter = 0
    return


def predict_lexicographer(input_path: str, output_path: str, resources_path: str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <lexicographerId>" format (e.g. "d000.s000.t000 noun.animal").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    global mfs_counter
    print(">>>> LEXICOGRAPHER PREDICTION")
    prediction_results, sentences_xml_elements, tokenizzatore = __predict(input_path, resources_path)
    vocab_label_lex = create_mapping_dictionary(resources_path, mode='lex')
    correctly_saved = 0
    filename = os.path.normpath(input_path)
    filename = filename.split(os.sep)[-1]
    bn2lex = get_bn2lex()
    filename = filename[:-3] + "lexicon.gold.key.txt"
    for index in tqdm(range(len(prediction_results))):
        correctly_saved += __write_result(filename,
                                          sentences_xml_elements[index],
                                          resources_path, output_path,
                                          prediction_results[index][2][0][1:-1],
                                          tokenizzatore,
                                          vocab=vocab_label_lex,
                                          enable_coarse_grained=3,
                                          vocab_for_coarse=bn2lex)

    print("Successfully saved {} out of {}".format(correctly_saved, len(prediction_results)))
    del prediction_results
    print("Of these, {} were MFS".format(mfs_counter))
    mfs_counter = 0
    return


def __predict(input_path: str, resources_path: str) -> Tuple:
    """
    Actually predicts a sentence and returns the predictions in the requested formats
    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: The actual prediction by the network
    """
    train, etree_data = load_dataset(input_path)
    train = [dato for dato in train if dato]

    bert = Bert()
    tokenizatore = bert.tokenizer
    sentences_xml_elements = etree_data.xpath("/*/*/*")
    to_return = []
    for sent in tqdm(train):
        tok = tokenization_strategy(sent, tokenizatore) + ["[SEP]"]
        tok = tokenizatore.convert_tokens_to_ids(tok)
        sb = [0] * len(tok)
        prediction = bert.predict(np.array([tok]),np.array([sb]))
        to_return.append(prediction)
    return to_return,sentences_xml_elements, tokenizatore


def __write_result(filename: str,
                   frase,
                   resources_path: str,
                   outputh_path: str,
                   predictions,
                   tokenizer,
                   sense_dictionary: Dict,
                   vocab=None,
                   enable_coarse_grained: int = 1,
                   vocab_for_coarse=None) -> int:
    """
    Write results in the file system
    :param filename: the name of the file to save
    :param frase: the object from which recover the sentence
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :param output_path: the path of the output file (where you save your predictions)
    :param predictions: the predictions made by the system
    :param vocab: the vocab needed for giving a sense
    :param enable_coarse_grained: changes the flow of the function from fine-grained to coarse-grained. Default to 1. Possible values:
        1 --> Means I'm predicting with Babelnet. No extra precautions needed
        2 --> Means I'm predicting with WordNet Domains. Need to consult the vocab. If I don't find anything, the empty class "factotum" is returned instead
        3 --> Means I'm predicting with Lexicon. Need to consult the vocab.
    :param vocab_for_coarse: The vocab in support of mode 2 or 3
    :return: 1 if succeeds
    """
    global mfs_counter
    bn2wn = get_bn2wn()
    lemma2wn = reload_word_mapping(resources_path + "/mapping/lemma2wn.txt")
    to_write = []
    count = 0
    for parola in frase:
        name = parola.xpath('name()')
        if name == 'instance':
            id = parola.get('id')
            list_of_possible_senses_first_step = lemma2wn.get(parola.text)
            if not list_of_possible_senses_first_step:
                # MFS
                count += len(tokenizer.tokenize(parola.text))
                the_actual_meaning = MFS(parola,
                                         bn2wn,
                                         vocab2=vocab_for_coarse,
                                         pred_case=enable_coarse_grained)
                mfs_counter += 1
                if the_actual_meaning == None:
                    continue
                to_write.append((id, the_actual_meaning))
                continue
            list_of_possible_senses_bn_version = convert_from_wnlist_2_bnlist(list_of_possible_senses_first_step, bn2wn)

            # avoid duplicates

            list_of_possible_senses_bn_version = set(list_of_possible_senses_bn_version)

            pooled_stuff = np.mean(
                predictions[count: count + len(tokenizer.tokenize(parola.text))], axis=0
            )
            count += len(tokenizer.tokenize(parola.text))

            the_actual_meaning = None
            try:
                list_of_sense_embeddings = [sense_dictionary[parola.get('lemma')][senso]['embeddings'][0] for senso in list_of_possible_senses_bn_version]
                prova = np.array(list_of_sense_embeddings)
                prob_sensi = cosine_similarity(predictions[count: count + len(tokenizer.tokenize(parola.text))], prova)
                the_actual_meaning = list(list_of_possible_senses_bn_version)[np.argmax(prob_sensi)]
            except:
                pass
            # MFS
            mfs_counter += 1
            the_actual_meaning = MFS(parola,
                                     bn2wn,
                                     vocab2=vocab_for_coarse,
                                     pred_case=enable_coarse_grained)
            if the_actual_meaning == None:
                continue
            to_write.append((id, the_actual_meaning))
        else:
            count += len(tokenizer.tokenize(parola.text))
    with open(outputh_path + "/" + filename, "a") as test_saving:
        for tupla in to_write:
            test_saving.write(tupla[0] + " " + tupla[1] + "\n")
    del to_write
    del lemma2wn
    del bn2wn
    return 1



def MFS(parola, vocab: Dict, vocab2: Dict = None, pred_case: int = 1) -> str or None:
    """
    Returns the sense by applying the Most Frequent Sense (MFS) strategy
    :param parola: the Element object to which associate a sense
    :param vocab: the vocab needed for giving a sense
    :param vocab2: default to None. The other vocabulary to use if coarse-grained mode is enabled. Has to be populated if enable_coarse_grained
    :param pred_case: whether to adopt a "rollback" strategy such as MFS or not. Possible values:
        1 --> Means I'm predicting with Babelnet. No extra precautions needed
        2 --> Means I'm predicting with WordNet Domains. Need to consult the vocab. If I don't find anything, the empty class "factotum" is returned instead
        3 --> Means I'm predicting with Lexicon. Need to consult the vocab.
    :return: the chosen sense with the MFS technique
    """
    pos = parola.get('pos')
    pos_input = __decide_pos(pos)
    wordnet_object = wordnet.synsets(parola.get('lemma'), pos=pos_input)
    try:
        wordnet_object = wordnet_object[0]
    except:
        print(wordnet_object)
        print(parola.text)
        return None
    wn_synset = "wn:" + str(wordnet_object.offset()).zfill(8) + wordnet_object.pos()
    the_actual_meaning = next(key for key, value in vocab.items() if wn_synset in value)
    to_return = __extrapolate_value_for_MFS(the_actual_meaning, vocab=vocab2, pred_case=pred_case)
    return to_return


def __extrapolate_value_for_MFS(value: object, pred_case: int = 1, vocab: Dict = None) -> str:
    """
    Taking either a List or String in input, that represents the found Babelnet ID, this function handles it and return a string that contains the value of the prediction
    :param value: The Value from which to extrapolate the actual meaning found
    :param pred_case: whether to adopt a "rollback" strategy such as MFS or not. Possible values:
        1 --> Means I'm predicting with Babelnet. No extra precautions needed
        2 --> Means I'm predicting with WordNet Domains. Need to consult the vocab. If I don't find anything, the empty class "factotum" is returned instead
        3 --> Means I'm predicting with Lexicon. Need to consult the vocab.
    :param vocab: The vocab in support of mode 2 or 3.
    :return: the actual meaning found with MFS
    """
    the_meaning_to_explot = __type_checker(value)
    if pred_case == 1:
        return the_meaning_to_explot
    if pred_case == 2:
        to_return = vocab.get(the_meaning_to_explot)
        return to_return[0] if to_return else "factotum"
    if pred_case == 3:
        to_return = vocab.get(the_meaning_to_explot)
        return to_return[0]


def __type_checker(value: object) -> str:
    """
    Checks the type of the object and, accordingly, returns it
    :param value: the value to examinate
    :return: a string that is the value expected
    """
    if type(value) == str:
        return value
    if type(value) == list:
        return value[0]


def __decide_pos(pos: str) -> str:
    """
    Decides the WN representation of the given pos in input
    :param pos: the pos to interpret with WordNet
    :return: the WN representation of the given pos
    """
    to_return = None
    if pos == 'NOUN':
        to_return = "n"
    if pos == 'VERB':
        to_return = 'v'
    if pos == 'ADJ':
        to_return = 'a'
    if pos == 'ADV':
        to_return = 'r'
    return to_return


def convert_from_wnlist_2_bnlist(list_of_bn: List, vocab: Dict) -> List:
    """
    Cast the given list (which contains only WN ids) to Babelnet IDs
    :param list_of_bn: the list to cast
    :param vocab: the vocabulary to use to perform the conversion
    :return: the converted list
    """
    list_of_possible_senses_bn_version = []
    for candidate in list_of_bn:
        is_it_here = next(key for key, value in vocab.items() if candidate in value)
        if is_it_here:
            if type(is_it_here) == str:
                list_of_possible_senses_bn_version.append(is_it_here)
            if type(is_it_here) == list:
                list_of_possible_senses_bn_version.extend(is_it_here)
    return list_of_possible_senses_bn_version


def create_custom_label(list_of_possible_senses: List,
                        word: str, vocab: Dict, predictions,
                        enable_coarse_grained: int = 1,
                        vocab_for_coarse: Dict = None) -> Tuple:
    """
    Converts the list of babelnet IDS to a number and outputs the converted list
    :param list_of_possible_senses: the list that contains all the babelnet's IDs
    :param word: the word for which we are predicting the sense in a specific moment
    :param vocab: the vocabulary Word -> Serial to exploit for the conversion
    :param predictions: the predictions made by the system
    :param enable_coarse_grained: changes the flow of the function from fine-grained to coarse-grained. Default to None. Possible values:
        1 --> The flow will still be the same
        2,3 -> Flow will change, triggering the first step for the coarse-grained approach.
    :param vocab_for_coarse: The vocab in support of mode 2 or 3
    :return: a Tuple containing the List with the IDs converted and the List of possible sensed updated
    """
    to_return = []
    list_of_indices_to_delete = []
    for sense in list_of_possible_senses:
        conversion = None
        try:
            conversion = int(vocab[word]) if enable_coarse_grained == 1 else vocab_for_coarse[sense][0]
            conversion = int(vocab[conversion]) if enable_coarse_grained != 1 else conversion
            to_return.append(predictions[conversion])
        except:
            list_of_indices_to_delete.append(sense)
            continue
    if list_of_indices_to_delete:
        list_of_possible_senses = [senso for senso in list_of_possible_senses if senso not in list_of_indices_to_delete]
    return to_return, list_of_possible_senses




if __name__ == "__main__":
    predict_babelnet("/Users/gimmi/Desktop/Università/MAGISTRALE/NLP/nlp-finalproject/dataset/eval_dataset/semeval2015.it.data.xml",
                     "../output", "/Users/gimmi/Desktop/Università/MAGISTRALE/NLP/nlp-finalproject/resources")
    predict_wordnet_domains(
        "/Users/gimmi/Desktop/Università/MAGISTRALE/NLP/nlp-finalproject/dataset/test/senseval2.data.xml", "../output",
        "/Users/gimmi/Desktop/Università/MAGISTRALE/NLP/nlp-finalproject/resources")
    predict_lexicographer(
        "/Users/gimmi/Desktop/Università/MAGISTRALE/NLP/nlp-finalproject/dataset/test/senseval2.data.xml", "../output",
        "/Users/gimmi/Desktop/Università/MAGISTRALE/NLP/nlp-finalproject/resources")
