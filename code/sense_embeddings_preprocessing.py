from typing import Dict
from nltk.corpus import wordnet
from data_preprocessing import __load_synset_mapping
import numpy as np


def sensembert_generator(sense_path:str):
    """
    Creates a generator for reading the SensEmBERT file
    :param sense_path: the path to the SensEmBERT file
    :return: a generator created from the stream of SensEmBERT
    """
    with open(sense_path) as sensembert:
        for line in sensembert.readlines()[1:]:
            yield line

def load_senses(sense_path:str, resources_path: str, is_it_english: bool = True) -> Dict:
    """
    Loads the given SensEmBERT file from input and returns it as a dictionary
    :param sense_path: the path to the SensEmBERT file
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :param is_it_english: whether the system will perform WSD on an english corpora. Default is true
    :return: a dictionary containing the SensEmBERT infos
    """
    sense_dict = dict()
    bn2wn = __load_synset_mapping(resources_path+'/babelnet2wordnet.tsv')
    with open(sense_path) as sensembert:
        tmp = sensembert.readlines()[1:]
    for elemento in sensembert_generator(sense_path):
        contenuto = elemento.split(" ")
        chiave_completa = contenuto[0].split("%")
        lemma = chiave_completa[0]
        wordnet_synset = None
        synset_id = None
        bn = None
        if is_it_english:
            #Converting the synset key if the task is english
            wordnet_synset = wordnet.lemma_from_key(contenuto[0]).synset()
            synset_id = "wn:" + str(wordnet_synset.offset()).zfill(8) + wordnet_synset.pos()
            bn = next(chiave for chiave, valore in bn2wn.items() if synset_id in valore)
        else:
            #Don't need to conver the synset key: it's already in BabelNet!
            bn = chiave_completa[1]
        #Load the scores...
        scores = np.loadtxt(contenuto[1:])
        #...and insert everything inside the dictionaruy
        if lemma not in sense_dict:
            sense_dict[lemma] = dict()
        if bn not in sense_dict[lemma]:
            sense_dict[lemma][bn] = dict()
            sense_dict[lemma][bn]['embeddings'] = []
        sense_dict[lemma][bn]['embeddings'].append(scores)

    return sense_dict
